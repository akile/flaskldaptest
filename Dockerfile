# FROM centos:7
FROM gitlab-registry.cern.ch/linuxsupport/cc7-base

RUN yum install -y \
    python36 \
    wget gcc make \
    bzip2-devel zlib-devel epel-release python36-pip libaio 
    
# Install the requirements for running the application
COPY requirements.txt /tmp/requirements.txt
RUN pip3.6 install --trusted-host pypi.python.org -r /tmp/requirements.txt

# Install the Oracle dependencies.
RUN yum -y --enablerepo cernonly install oracle-instantclient11.2-basiclite oracle-instantclient11.2-meta oracle-instantclient-tnsnames.ora.noarch

# Sets the environmental variable LD_LIBRARY_PATH to the appropriate directory for the Instant Client Version    
# ENV LD_LIBRARY_PATH /usr/lib/oracle/18.3/client64/lib:${LD_LIBRARY_PATH}
