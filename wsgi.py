from flask import Flask, request,jsonify, redirect, json
from ldap3 import Server, Connection, ALL, ALL_ATTRIBUTES
import cx_Oracle
import sqlalchemy as db
import os


application = Flask(__name__) 

engine = db.create_engine("oracle+cx_oracle://logbook_ti_dev:"+ os.environ['databasepassword'] + "@devdb11-s.cern.ch:10121/DEVDB112")


@application.route('/', methods=['GET'])
def index():
    print('called')
    # In order to fetch the username after the SSO login
    # username = request.headers['X-Remote-User']
    # print(request.headers)

    # server = Server("xldap.cern.ch")
    # base_dn = "OU=Users,OU=Organic Units,DC=cern,DC=ch"
    # conn = Connection(server, auto_bind=True)
    # search_filter = "(CN=" + username + ")"
    # result = conn.search(search_base=base_dn, search_filter=search_filter, attributes=ALL_ATTRIBUTES)
    # print(conn.entries[0])
    
    try:
        conn = engine.connect()
        query = 'SELECT * from USER_TEST'
        ResultProxy = conn.execute(query)
        ResultSet = ResultProxy.fetchall()
        usersList = []
        for users in ResultSet:
            user = {
                "ID": users[0],
                "USERNAME": users[1],
                "EMAIL": users[2]
            }
            usersList.append(user)
        return json.dumps(usersList)
    except Exception as e:
        print('Failed at getting users: ' + str(e))
        return 'Error'


@application.route('/test', methods=['GET'])
def site():
    return 'This should render'


if __name__ == '__main__':
    application.run(host='0.0.0.0', port=8080)


